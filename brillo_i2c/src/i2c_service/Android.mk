# Copyright 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := i2c_service
LOCAL_REQUIRED_MODULES := init.i2c_service.rc
LOCAL_SRC_FILES := i2c_service.cpp i2c_device.cpp
LOCAL_SHARED_LIBRARIES := libc libbase
LOCAL_CFLAGS := -Werror
include $(BUILD_EXECUTABLE)
