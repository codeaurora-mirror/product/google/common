Brillo Example Service
======================

This folder contains an example Brillo service and client that use AIDL. This
example is intended to show:

1. Implementing an interface defined in AIDL.
2. Registering a service with the service manager.
3. Calling a service from a client.
4. Registering a callback object from the client so the service can talk back
     to the client.

Folder layout
-------------

* android/brillo/example/IExampleService.aidl - AIDL interface for an example
  service that accepts a string and logs it. The service also provides an
  interface to let a client be notified once it has called the service n
  times.
* android/brillo/example/IAlertCallback.aidl - AIDL interface for the callback
  object that the client registers with the service. Note the use of the
  keyword oneway in the interface. This keyword prevents the service from
  blocking on the client handling the callback.
* brillo_example_service.cpp - The implementation of the AIDL service
  interface. The service has to implement
  android::brillo::example::BnExampleService which is done in ExampleService
  class. The service also illustrates how to use brillo::BinderWatcher with a
  message loop to drive the service.
* brillo_example_client.cpp - Client application that calls the service and
  receives callbacks. The client implements the callback object in
  AlertCallback. AlertCallback by extending
  android::brillo::example::BnAlertCallback. The client also uses
  brillo::BinderWatcher and message loops to receive callbacks.

The brillo_example_client reads std::in and passes that information to the
brillo_example_service which logs it using logcat.

Usage
-----

To use this example service/client:

1. Open two terminals.
2. In the first one, run

        adb shell

    and then start the example service in the background (not required if the
    service is started by init)

        brillo_example_service &

3. Start logcat in the second terminal

        adb logcat

4. In the first terminal, start the example client as root.

        brillo_example_client

5. If you type "foo" in the first terminal followed by a newline, you should see
   the following text in the logcat output:

        brillo_example_service: foo

6. After 3 messages have been sent to the service by the client, the following
   statement will be logged by the client.

        brillo_example_client: Received Callback from service: Received 3 log messages.

